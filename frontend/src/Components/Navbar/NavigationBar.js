import React, { PureComponent } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";

const { Toggle, Collapse, Brand } = Navbar;
const NavLink = Nav.Link;

class NavigationBar extends PureComponent {

  closeSession = () => {
    localStorage.clear()
    window.location = "/"
  }

  render() {

    return (
      <Navbar collapseOnSelect sticky="top" expand="sm" variant="dark" className="habi-navbar">
        <Container>
          <Brand><NavLink href="/" className="habi-navbar__brand">Habi</NavLink> </Brand>
          <Toggle aria-controls="responsive-navbar-nav" />
          <Collapse id="responsive-navbar-nav" className="justify-content-end">
            <Nav>
              <a className="habi-navbar__link-end habi-navbar__link-end" href="/">Log In / Log Out</a>
            </Nav>
          </Collapse>
        </Container>
      </Navbar>
    );
  }
}

export default NavigationBar;
