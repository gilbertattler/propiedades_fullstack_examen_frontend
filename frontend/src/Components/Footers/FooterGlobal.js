import React from 'react'

const FooterGlobal = () => {
    return (
        <>
            <footer className="fixed-footer">
                <div className="fixed-footer__info">powered by <a href="/">Habi</a></div>
            </footer>
        </>
    )
}

export default FooterGlobal