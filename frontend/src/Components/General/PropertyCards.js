import React from 'react';
import PropTypes from 'prop-types';import Property from './Property'
import '../../styles/components/propertyCards.scss';

const PropertyCards = ({ properties }) => {
    return (
        <>
            {properties && properties.length ? (
                <ul className="property-cards">
                    {properties.map((property) => (
                        <li key={property.id} className="property-cards__item">
                            <Property {...property}/>
                        </li>
                    ))}
                </ul>
            ) : (
             <div></div>
            )}
        </>
    );
}

PropertyCards.propTypes = {
    properties: PropTypes.array,
}

export default PropertyCards;
