import React, { Component } from 'react';
import PropTypes from 'prop-types';
import home from '../../images/home.png'
import '../../styles/components/property.scss';
import { faBed, faBath, faHouse, faPen, faTrash, faEye } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Row, Col } from "react-bootstrap";

class Property extends Component {
    render() {
        const { name, price, description, cp, state, city } = this.props;
        const format_currency = (priceValue) => {
            return `$ ${priceValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")} MXN`;
        }
        return (
            <div className="property">
                <img src={home} alt="property" className="property__image"/>
                <Row className="property__icon-wrap">
                    <Col>
                        <FontAwesomeIcon
                            icon={faBed}
                            className="property__icon-info"
                            size="sm"
                        />
                    </Col>
                    <Col>
                        <FontAwesomeIcon
                            icon={faBath}
                            className="property__icon-info"
                            size="sm"
                        />
                    </Col>
                    <Col>
                        <FontAwesomeIcon
                            icon={faHouse}
                            className="property__icon-info"
                            size="sm"
                        />
                    </Col>
                </Row>
                <hr className="property__separator" />
                <div className="property__body">
                    <div className="property__title">{`${name} ${city}, ${state} CP ${cp}`}</div>
                    <div className="property__price">{format_currency(price)}</div>
                    <div className="property__description">
                        <span className="property__dot"></span>{description}
                    </div>
                    <Row className="property__buttons-wrap">
                        <Col>
                            <a href="/" className="property__button">
                                <FontAwesomeIcon
                                    icon={faEye}
                                    className="property__button-action"
                                    size="lg"
                                />
                            </a>
                        </Col>
                        <Col>
                            <a href="/" className="">
                                <FontAwesomeIcon
                                    icon={faPen}
                                    className="property__button-action"
                                    size="lg"
                                />
                            </a>
                        </Col>
                        <Col>
                            <a href="/" className="">
                                <FontAwesomeIcon
                                    icon={faTrash}
                                    className="property__button-action"
                                    size="lg"
                                />
                            </a>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }
}

Property.propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    neighborhood: PropTypes.string,
    city: PropTypes.string,
    cp: PropTypes.string,
    state: PropTypes.string,
}

export default Property;
