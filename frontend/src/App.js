import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/Home";
import Page404 from "./Pages/Page404";
import LayoutGlobal from './Layouts/LayoutGlobal'

const App = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route element={<LayoutGlobal />}>
            <Route element={<HomePage/>} path="/" />
            <Route path="*" element={<Page404 />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
};

export default App;
