import React from 'react'
import NavigationBar from "../Components/Navbar/NavigationBar";
import FooterGlobal from '../Components/Footers/FooterGlobal'
import { Outlet } from "react-router-dom";

const LayoutGlobal = () => {
    return (
        <>
            <NavigationBar />
            <main>
                <section>
                    <Outlet />
                    <FooterGlobal />
                </section>
            </main>
        </>
    )
}

export default LayoutGlobal
