import { connect } from 'react-redux';
import PropertyCards from '../Components/General/PropertyCards';

const mapStateToProps = (state) => ({
    properties: state.propertyReducer.properties,
});
  
export default connect(mapStateToProps, {})(PropertyCards);