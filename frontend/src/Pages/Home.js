import React, { useEffect } from "react";
import { Container } from "react-bootstrap";
import { connect } from "react-redux";
import store from "../redux/store";
import { getAllProperties } from "../redux/actionCreators";
import PropertyCards from '../Containers/PropertyCards';

const HomePage = ({ properties }) => {
  useEffect(() => {
    store.dispatch(getAllProperties());
  }, []);

  return (
    <div>
      <Container>
        <div>
          <Container>
            <PropertyCards />
          </Container>
        </div>
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  properties: state.propertyReducer.properties,
});

export default connect(mapStateToProps, {})(HomePage);
