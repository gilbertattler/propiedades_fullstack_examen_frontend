import React from 'react'

const Page404 = () => (
  <div class="page-header text-center mt-5">
    <h1>404 Not found</h1>
  </div>
)

export default Page404
